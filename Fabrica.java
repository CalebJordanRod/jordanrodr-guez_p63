import javax.swing.JOptionPane; 

public class Fabrica {
		
	public static void main(String[] args) {
		boolean end = false;
		String n;
		Coche [] arrayco = new Coche[5];
		
		do{
		n = JOptionPane.showInputDialog(null, "Fabrica de coches Jordan Rodríguez. Selecciona una de las siguientes opciones:\n" +
		"1. Fabricar coche (conociendo la matrícula).\n" +
		"2. Fabricar coche (a partir del número de puertas y plazas).\n" +
		"3. Fabricar coche (a partir de la marca, modelo y color).\n" +
		"4. Fabricar coche (sin datos).\n" +
		"5. Tunear coche (pintándolo de un color).\n" +
		"6. Tunear coche (sin pintarlo).\n" +
		"7. Avanzar kilómetros. \n" +
		"8. Mostrar características de un coche.\n" +
		"9. Salir del programa.\n\n","¡Bienvenido a mi fábrica de coches!", JOptionPane.PLAIN_MESSAGE);
		

		switch (n)
		{
		case "1": 
		{
			if (Coche.getNumCoches() < Coche.getMaxCoches()){
			String m;
			m = JOptionPane.showInputDialog(null, "Inserte matrícula");
			Coche coche = new Coche(m);
			arrayco[Coche.getNumCoches()-1] = coche;
			System.out.println(Coche.getNumCoches());
			caracteristicas(coche);
			}else {
				JOptionPane.showMessageDialog(null, "El almacen de coches está lleno");
			}
			break;
		}
		case "2":
		{
			if (Coche.getNumCoches() < Coche.getMaxCoches()){
			String ps,pls;
			int p,pl;
			ps = JOptionPane.showInputDialog(null, "Inserte número de puertas:");
			p = Integer.parseInt(ps);
			pls = JOptionPane.showInputDialog(null, "Inserte el número de plazas:");
			pl = Integer.parseInt(pls);
			Coche coche1 = new Coche (p,pl);
			coche1.setMatricula(matAleatoria());
			caracteristicas(coche1);
			arrayco[Coche.getNumCoches()-1] = coche1;
			System.out.println(Coche.getNumCoches());
			}else {
				JOptionPane.showMessageDialog(null, "El almacen de coches está lleno");
			}
			break;
		}
		case "3":
		{
			if (Coche.getNumCoches() < Coche.getMaxCoches()){
			String m, mo, co;
			m = JOptionPane.showInputDialog(null,"Inserte marca del coche:");
			mo = JOptionPane.showInputDialog(null, "Inserte modelo del coche:");
			co = JOptionPane.showInputDialog(null, "Inserte color del coche:");
			Coche coche2 = new Coche (m,mo,co);
			coche2.setMatricula(matAleatoria());
			caracteristicas(coche2);
			arrayco[Coche.getNumCoches()-1] = coche2;
			System.out.println(Coche.getNumCoches());
			}else {
				JOptionPane.showMessageDialog(null, "El almacen de coches está lleno");
			}
			break;
		}
		case "4":
		{
			if (Coche.getNumCoches() < Coche.getMaxCoches()){
			Coche coche3 = new Coche();
			System.out.println(Coche.getNumCoches());
			coche3.setMatricula(matAleatoria());
			caracteristicas(coche3);
			arrayco[Coche.getNumCoches()-1] = coche3;
			}else {
				JOptionPane.showMessageDialog(null, "El almacen de coches está lleno");
			}
			break;
		}
		case "5":
		{
			if (Coche.getNumCoches() == 0){
				JOptionPane.showMessageDialog(null, "No hay coches en el almacen");
			}else
            {
                String t = JOptionPane.showInputDialog("Introduce matricula coche a modificar");
                int b = buscaCoche(arrayco, t);
                if(b == -1)
                {
                    JOptionPane.showMessageDialog(null, "La matricula introducida no coincide con ningun coche");
                } else
                {
                    String tl = JOptionPane.showInputDialog("Introduce el color");
                    arrayco[b].tunear(tl);
                    caracteristicas(arrayco[b]);
                }
            }
            break;
		}
		case "6":
		{
			if (Coche.getNumCoches() == 0){
				JOptionPane.showMessageDialog(null, "No hay coches en el almacen");
			}else{
				String t = JOptionPane.showInputDialog(null,"Introduce la matrícula del coche a modificar: ");
				int b = buscaCoche(arrayco, t);
				if(b == -1){
					JOptionPane.showMessageDialog(null, "La matricula introducida no coincide con ningun coche");
				}else{
					arrayco[b].tunear();
					caracteristicas(arrayco[b]);
				}
			}
			break;
		}
		case "7":
		{
			if (Coche.getNumCoches() == 0){
				JOptionPane.showMessageDialog(null, "No hay coches en el almacen");
			}else{
				String t = JOptionPane.showInputDialog(null,"Introduce la matrícula del coche a modificar: ");
				int b = buscaCoche(arrayco, t);
				if(b == -1){
					JOptionPane.showMessageDialog(null, "La matricula introducida no coincide con ningun coche");
				}else{
					String dou = JOptionPane.showInputDialog(null, "Introduce los kilómetros que desee avanzar: ");
					double doub = Double.parseDouble(dou);
					arrayco[b].avanzar(doub);
					caracteristicas(arrayco[b]);
				}
				}
			break;
		}
		case "8":
		{
			if (Coche.getNumCoches() == 0){
				JOptionPane.showMessageDialog(null, "No hay coches en el almacen");
			}else{
				String t = JOptionPane.showInputDialog(null,"Introduce la matrícula del coche a modificar: ");
				int b = buscaCoche(arrayco, t);
				if(b == -1){
					JOptionPane.showMessageDialog(null, "La matricula introducida no coincide con ningun coche");
				}else{
					caracteristicas(arrayco[b]);
				}
			}
			break;
		}
		case "9":
		{
			JOptionPane.showMessageDialog(null, "¡Hasta pronto!");
			end = true;
			break;
		}
		}
		}while(end == false);
		
	}
	public static void caracteristicas(Coche x){
		JOptionPane.showMessageDialog(null, "Los datos del coche son los siguientes: \n"+
		"Matricula: "+ x.getMatricula() +
		"\nMarca: "+ x.getMarca() +
		"\nModelo: "+ x.getModelo() +
		"\nColor: "+ x.getColor() +
		"\nKilometros: "+ x.getKilometros() +
		"\nTecho Solar: "+ x.getTechoSolar() +
		"\nNumero de puertas: "+ x.getNumPuertas() +
		"\nNumero de plazas: "+ x.getNumPlazas() +
		"\n");
	}
	public static String matAleatoria(){
		int r = (int) (1 + Math.random()*9);
		int a = (int) (1 + Math.random()*9);
		int n = (int) (1 + Math.random()*9);
		int d = (int) (1 + Math.random()*9);
		int o = (int) (1 + Math.random()*9);
		String m = (r + "" + a + "" + n + "" + d + "" + o);
		return m;
		
	}
    public static int buscaCoche(Coche arrayco[], String m)
    {
        for(int x = 0; x < arrayco.length; x++){
        	if (arrayco[x] == null){
        		return -1;
        	}
            if(m.equals(arrayco[x].getMatricula())){
                return x;
            }
        }
        return -1;
    }

}