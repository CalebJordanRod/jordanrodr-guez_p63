import javax.swing.JOptionPane; 

class Coche {
	
	private static int numcoches = 0;
	private static final int MAX_COCHES = 5;
	private String matricula = "";
	private String marca = "SEAT";
	private String modelo = "ALTEA";
	private String color = "Blanco";
	private boolean techoSolar = false;
	private double kilometros = 0.0;
	private int numPuertas = 3;
	private int numPlazas = 5;
	
	public Coche(){
        matricula = "";
        marca = "SEAT";
        modelo = "ALTEA";
        color = "Blanco";
        techoSolar = false;
        kilometros = 0.0;
        numPuertas = 3;
        numPlazas = 5;
        numcoches++;
	}
	
	public Coche (String matricula){
		this.matricula = matricula;
		numcoches ++;
	}
	public Coche (int numPuertas, int numPlazas){
		this.numPuertas = numPuertas;
		this.numPlazas = numPlazas;
		numcoches ++;
	}
	public Coche (String marca, String modelo, String color){
		this.marca = marca;
		this.modelo = modelo;
		this.color = color;
		numcoches ++;
	}
	public static int getNumCoches(){
		return numcoches;
	}
	public static int getMaxCoches(){
		return MAX_COCHES;
	}
	public void setMatricula (String matricula){
		this.matricula = matricula;
	}
	public String getMatricula (){
		return matricula;
	}
	public void setMarca (String marca){
		this.marca = marca;
	}
	public String getMarca(){
		return marca;
	}
	public void setModelo(String modelo){
		this.modelo = modelo;
	}
	public String getModelo(){
		return modelo;
	}
	public void setColor(String color){
		this.color = color;
	}
	public String getColor(){
		return color;
	}
	public void setTechoSolar(boolean techoSolar){
		this.techoSolar = techoSolar;
	}
	public String getTechoSolar (){
		if (techoSolar == true){
			return "Incorporado";
		}
		else {
			return "No incorporado";
		}
	}
	public void setKilometros(double kilometros){
		this.kilometros = kilometros;
	}
	public double getKilometros(){
		return kilometros;
	}
	public void setNumPuertas (int numPuertas){
		this.numPuertas = numPuertas;
	}
	public int getNumPuertas (){
		return numPuertas;
	}
	public void setNumPlazas(int numPlazas){
		this.numPlazas = numPlazas;
	}
	public int getNumPlazas (){
		return numPlazas;
	}
	public void avanzar (double kilometros){
		this.kilometros = kilometros;
		JOptionPane.showMessageDialog (null,"El cuentakilometros esta en "+kilometros);
	}
	public void tunear(){
		kilometros = 0;
		if (techoSolar == false){
			techoSolar = true;
			JOptionPane.showMessageDialog(null, "Se le ha reiniciado el cuentakilometros y aplicado un techo solar a su coche.");
		}
		else {
			JOptionPane.showMessageDialog(null, "Se le ha reiniciado el cuentakilometros a su coche.");
		}
	}
	public void tunear (String color){
		this.color = color;
		kilometros = 0;
		if (techoSolar == false){
			techoSolar = true;
			JOptionPane.showMessageDialog (null, "Se le ha aplicado el color deseado, reiniciado el cuentakilometros y aplicado un techo solar a su coche.");
		}
		else {
			JOptionPane.showMessageDialog (null, "Se le ha aplicado el color deseado y reiniciado el cuentakilometros.");
		}
	}
	public void matricular(String matricula){
		this.matricula = matricula;
		JOptionPane.showMessageDialog(null, "Se le ha asignado al coche la matricula deseada.");
	}
}